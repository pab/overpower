# OVERPOWER Subtractive Synthesizer

Repository for OVERPOWER, a subtractive software synthesizer made using National Instruments LabVIEW. It consists of two sound generators (called oscillators) and has facilities for creating sound effects like filtering, distortion and phase shifts. 

<p align="center">
  <img src="Overpower.png" />
</p>
  
## Installation

### Prerequisite

- [LabVIEW](https://www.ni.com/en-in/shop/labview.html) installation for the relevant OS. 

1. Open `OverpowerAdditiveSynth.vi` from within LabVIEW.
2. Execute `Run` option on Front Panel window.
3. Modulate the GUI controls as desired.
4. Sounds can be generated either using the `ASD` row of the computer keyboard or by enabling the `AUTOPLAY` and/or `Auto Music` buttons.

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.
